<?xml version="1.0" encoding="utf-8"?>
<tileset name="PrtCave" tilewidth="16" tileheight="16" tilecount="80" columns="16">
  <image source="../../Textures/PrtCave.png" width="256" height="80" />
  <tile id="1">
    <objectgroup draworder="index">
      <object id="1" x="0" y="0" width="16" height="16" />
    </objectgroup>
  </tile>
  <tile id="2">
    <objectgroup draworder="index">
      <object id="1" x="-0.24616" y="0.24616" width="0" height="0">
        <polygon points="0.24616,-0.24616 0.164107,15.8363 16.2462,7.75384 16.2466,-0.24616" />
      </object>
    </objectgroup>
  </tile>
  <tile id="3">
    <objectgroup draworder="index">
      <object id="2" x="0" y="0" width="0" height="0">
        <polygon points="0,0 0,8 16,0" />
      </object>
    </objectgroup>
  </tile>
  <tile id="4">
    <objectgroup draworder="index">
      <object id="2" x="0" y="0" width="0" height="0">
        <polygon points="0,0 16,8 16,0" />
      </object>
    </objectgroup>
  </tile>
  <tile id="5">
    <objectgroup draworder="index">
      <object id="2" x="0" y="0" width="0" height="0">
        <polygon points="0,0 0,8 16,16 16,0" />
      </object>
    </objectgroup>
  </tile>
  <tile id="6">
    <objectgroup draworder="index">
      <object id="1" x="0.410267" y="0.0820533" width="0" height="0">
        <polygon points="-0.410267,-0.0820533 15.5897,7.91795 15.5897,15.9179 -0.410267,15.9179" />
      </object>
    </objectgroup>
  </tile>
  <tile id="7">
    <objectgroup draworder="index">
      <object id="2" x="16" y="16" width="0" height="0">
        <polygon points="0,0 -16,-8 -16,0" />
      </object>
    </objectgroup>
  </tile>
  <tile id="8">
    <objectgroup draworder="index">
      <object id="3" x="0" y="16" width="0" height="0">
        <polygon points="0,0 16,-8 16,0" />
      </object>
    </objectgroup>
  </tile>
  <tile id="9">
    <objectgroup draworder="index">
      <object id="2" x="0" y="16" width="0" height="0">
        <polygon points="0,0 0,-8 16,-16 16,0" />
      </object>
    </objectgroup>
  </tile>
  <tile id="11">
    <objectgroup draworder="index">
      <object id="1" x="0" y="16" width="0" height="0">
        <polygon points="0,0 0,-16 16,-16" />
      </object>
    </objectgroup>
  </tile>
  <tile id="12" />
  <tile id="18">
    <objectgroup draworder="index">
      <object id="1" x="0" y="0" width="0" height="0">
        <polygon points="0,0 16,8 16,16 0,16" />
      </object>
    </objectgroup>
  </tile>
  <tile id="19">
    <objectgroup draworder="index">
      <object id="1" x="0" y="0" width="0" height="0">
        <polygon points="0,8 16,16 0,16" />
      </object>
    </objectgroup>
  </tile>
  <tile id="20">
    <objectgroup draworder="index">
      <object id="1" x="0" y="0" width="0" height="0">
        <polygon points="0,16 16,8 16,16" />
      </object>
    </objectgroup>
  </tile>
  <tile id="21">
    <objectgroup draworder="index">
      <object id="1" x="0" y="0" width="0" height="0">
        <polygon points="0,8 16,0 16,16 0,16" />
      </object>
    </objectgroup>
  </tile>
  <tile id="27" />
  <tile id="28">
    <objectgroup draworder="index">
      <object id="1" x="0" y="0" width="0" height="0">
        <polygon points="0,0 16,16 16,0" />
      </object>
    </objectgroup>
  </tile>
  <tile id="32">
    <objectgroup draworder="index">
      <object id="1" x="0" y="0" width="16" height="16" />
    </objectgroup>
  </tile>
  <tile id="33">
    <objectgroup draworder="index">
      <object id="1" x="0" y="0" width="16" height="16" />
    </objectgroup>
  </tile>
  <tile id="36">
    <objectgroup draworder="index">
      <object id="1" x="0.164107" y="0.164107" width="0" height="0">
        <polygon points="0,0 15.9183,7.3848 15.8363,-0.0820533" />
      </object>
    </objectgroup>
  </tile>
  <tile id="41">
    <objectgroup draworder="index">
      <object id="1" x="0" y="0" width="16" height="16" />
    </objectgroup>
  </tile>
  <tile id="42">
    <objectgroup draworder="index">
      <object id="1" x="0" y="0" width="16" height="16" />
    </objectgroup>
  </tile>
  <tile id="48">
    <objectgroup draworder="index">
      <object id="1" x="0" y="0" width="16" height="16" />
    </objectgroup>
  </tile>
  <tile id="49">
    <objectgroup draworder="index">
      <object id="1" x="0" y="0" width="16" height="16" />
    </objectgroup>
  </tile>
</tileset>