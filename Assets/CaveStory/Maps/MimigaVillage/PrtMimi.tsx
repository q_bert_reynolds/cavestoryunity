<?xml version="1.0" encoding="utf-8"?>
<tileset name="PrtMimi" tilewidth="16" tileheight="16" tilecount="160" columns="16">
  <image source="../../Textures/PrtMimi.png" width="256" height="160" />
  <tile id="18">
    <objectgroup draworder="index">
      <object id="1" x="0" y="16" width="0" height="0">
        <polygon points="0,0 16,-16 0,-16" />
      </object>
    </objectgroup>
  </tile>
  <tile id="21">
    <objectgroup draworder="index">
      <object id="1" x="0" y="0" width="0" height="0">
        <polygon points="0,0 16,16 16,0" />
      </object>
    </objectgroup>
  </tile>
  <tile id="34">
    <objectgroup draworder="index">
      <object id="1" x="0" y="0" width="16" height="16" />
    </objectgroup>
  </tile>
  <tile id="35">
    <objectgroup draworder="index">
      <object id="1" x="0" y="0" width="16" height="16" />
    </objectgroup>
  </tile>
  <tile id="36">
    <objectgroup draworder="index">
      <object id="1" x="0" y="0" width="16" height="16" />
    </objectgroup>
  </tile>
  <tile id="51">
    <objectgroup draworder="index">
      <object id="1" x="0" y="0" width="16" height="16" />
    </objectgroup>
  </tile>
  <tile id="52">
    <objectgroup draworder="index">
      <object id="1" x="0" y="0" width="16" height="16" />
    </objectgroup>
  </tile>
  <tile id="58">
    <objectgroup draworder="index">
      <object id="1" x="8" y="16" width="0" height="0">
        <polygon points="0,0 8,-16 8,0" />
      </object>
    </objectgroup>
  </tile>
  <tile id="59">
    <objectgroup draworder="index">
      <object id="1" x="0" y="0" width="16" height="16" />
    </objectgroup>
  </tile>
  <tile id="60">
    <objectgroup draworder="index">
      <object id="1" x="0" y="0" width="16" height="16" />
    </objectgroup>
  </tile>
  <tile id="61">
    <objectgroup draworder="index">
      <object id="1" x="0" y="0" width="16" height="16" />
    </objectgroup>
  </tile>
  <tile id="62">
    <objectgroup draworder="index">
      <object id="1" x="0" y="0" width="0" height="0">
        <polygon points="0,0 0,16 8,16" />
      </object>
    </objectgroup>
  </tile>
  <tile id="64">
    <objectgroup draworder="index">
      <object id="1" x="0" y="0" width="16" height="16" />
    </objectgroup>
  </tile>
  <tile id="65">
    <objectgroup draworder="index">
      <object id="1" x="0" y="0" width="16" height="16" />
    </objectgroup>
  </tile>
  <tile id="66">
    <objectgroup draworder="index">
      <object id="1" x="0" y="0" width="0" height="0">
        <polygon points="0,0 16,8 16,16 0,16" />
      </object>
    </objectgroup>
  </tile>
  <tile id="67">
    <objectgroup draworder="index">
      <object id="1" x="0" y="8" width="0" height="0">
        <polygon points="0,0 0,8 16,8" />
      </object>
    </objectgroup>
  </tile>
  <tile id="68">
    <objectgroup draworder="index">
      <object id="1" x="0" y="16" width="0" height="0">
        <polygon points="0,0 16,-8 16,0" />
      </object>
    </objectgroup>
  </tile>
  <tile id="69">
    <objectgroup draworder="index">
      <object id="1" x="0" y="8" width="0" height="0">
        <polygon points="0,0 16,-8 16,8 0,8" />
      </object>
    </objectgroup>
  </tile>
  <tile id="85">
    <objectgroup draworder="index">
      <object id="1" x="0" y="0" width="16" height="16" />
    </objectgroup>
  </tile>
  <tile id="91">
    <objectgroup draworder="index">
      <object id="1" x="0" y="0" width="0" height="0">
        <polygon points="0,0 16,8 16,16 0,16" />
      </object>
    </objectgroup>
  </tile>
  <tile id="92">
    <objectgroup draworder="index">
      <object id="1" x="0" y="7" width="0" height="0">
        <polygon points="0,0 16,9 0,9" />
      </object>
    </objectgroup>
  </tile>
  <tile id="94">
    <objectgroup draworder="index">
      <object id="1" x="0" y="16" width="0" height="0">
        <polygon points="0,0 16,-8 16,0" />
      </object>
    </objectgroup>
  </tile>
  <tile id="95">
    <objectgroup draworder="index">
      <object id="1" x="0" y="8" width="0" height="0">
        <polygon points="0,0 16,-8 16,8 0,8" />
      </object>
    </objectgroup>
  </tile>
  <tile id="107">
    <objectgroup draworder="index">
      <object id="1" x="0" y="0" width="0" height="0">
        <polygon points="0,0 16,8 16,16 0,16" />
      </object>
    </objectgroup>
  </tile>
  <tile id="108">
    <objectgroup draworder="index">
      <object id="1" x="0" y="7" width="0" height="0">
        <polygon points="0,0 16,9 0,9" />
      </object>
    </objectgroup>
  </tile>
  <tile id="110">
    <objectgroup draworder="index">
      <object id="1" x="0" y="16" width="0" height="0">
        <polygon points="0,0 16,-8 16,0" />
      </object>
    </objectgroup>
  </tile>
  <tile id="111">
    <objectgroup draworder="index">
      <object id="1" x="0" y="8" width="0" height="0">
        <polygon points="0,0 16,-8 16,8 0,8" />
      </object>
    </objectgroup>
  </tile>
  <tile id="123">
    <objectgroup draworder="index">
      <object id="1" x="0" y="16" width="0" height="0">
        <polygon points="0,0 16,-16 0,-16" />
      </object>
    </objectgroup>
  </tile>
  <tile id="127">
    <objectgroup draworder="index">
      <object id="1" x="0" y="0" width="0" height="0">
        <polygon points="0,0 16,16 16,0" />
      </object>
    </objectgroup>
  </tile>
</tileset>