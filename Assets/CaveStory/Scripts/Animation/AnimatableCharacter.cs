﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Paraphernalia.Components;

[RequireComponent(typeof(Interactable))]
[RequireComponent(typeof(Animator))]
public class AnimatableCharacter : MonoBehaviour {

	public int type;

	Animator anim;
	Interactable interactable;

	bool runningAway;
	
	void Awake () {
		anim = GetComponent<Animator>();
		interactable = GetComponent<Interactable>();
	}

	void OnDisable () {
		runningAway = false;
	}

	public void SetDirection (int dir) {
		interactable.SetDirection(dir);
	}

	public void PlayAnimation (int animID) {
		anim.SetInteger("animID", animID);
		anim.SetTrigger("animate");
	}

	public void Splat () {
		ParticleManager.Play("Poof", transform.position);
		AudioManager.PlayEffect("landHard");
	}

	public void KnockBack () {
		JumpController jump = GetComponent<JumpController>();
		if (jump) jump.Jump(1.5f * Vector3.Dot(Vector3.right, transform.right));
	}

	public void RunAway () {
		StartCoroutine("RunAwayCoroutine");
	}

	IEnumerator RunAwayCoroutine () {
		runningAway = true;

		JumpController jump = GetComponent<JumpController>();
		float x = Vector3.Dot(Vector3.right, transform.right);
		if (jump) jump.Jump(new Vector2(x * 0.3f, 0.3f));

		yield return new WaitForSeconds(0.1f);

		Rigidbody2D body = GetComponent<Rigidbody2D>();
		while (runningAway) {
			float y = body.velocity.y;
			body.velocity = transform.right * 8.5f + Vector3.up * y;
			yield return new WaitForEndOfFrame();
		}
	}

	void OnCollisionEnter2D (Collision2D c) {
		anim.SetInteger("animID", -1);
		anim.SetTrigger("collision");

		if (runningAway) {
			foreach (ContactPoint2D cp in c.contacts) {
				float ang = Vector2.Angle(cp.normal, Vector2.up);
				if (ang > 85 && ang < 95) {
					Rigidbody2D body = GetComponent<Rigidbody2D>();
					transform.localEulerAngles += Vector3.up * 180;
					float x = body.velocity.x;
					body.velocity = new Vector2(x, -10);
					break;
				}
			}
		}
	}
}
