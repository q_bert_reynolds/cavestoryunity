﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomAnimator : MonoBehaviour {

	public float minInterval = 0.5f;
	public float maxInterval = 1;

	Animator anim;

	void Awake () {
		anim = GetComponent<Animator>();
	}

	void OnEnable () {
		StartCoroutine("UpdateRandom");
	}

	IEnumerator UpdateRandom () {
		while (enabled) {
			anim.SetTrigger("trigger");
			float interval = Random.Range(minInterval, maxInterval);
			yield return new WaitForSeconds(interval);
		}
	}
}
