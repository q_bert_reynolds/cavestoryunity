﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SequenceAnimator : MonoBehaviour {

	public AnimatorUpdateMode updateMode = AnimatorUpdateMode.Normal;
	public Sprite[] sprites;
	public float interval = 0.1f;

	SpriteRenderer spriteRenderer;

	void Awake () {
		spriteRenderer = GetComponent<SpriteRenderer>();
	}

	void OnEnable () {
		StartCoroutine("CycleSprites");
	}

	IEnumerator CycleSprites () {
		int i = 0;
		while (enabled) {
			spriteRenderer.sprite = sprites[i];
			i = (i + 1) % sprites.Length;
			if (updateMode == AnimatorUpdateMode.UnscaledTime) {
				yield return new WaitForSecondsRealtime(interval);
			}
			else {
				yield return new WaitForSeconds(interval);
			}
		}
	}
}
