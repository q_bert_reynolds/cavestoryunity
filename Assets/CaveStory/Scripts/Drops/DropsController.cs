﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Paraphernalia.Extensions;

public class DropsController : MonoBehaviour {

	public float dropChance = 0.7f;
	public string[] prefabNames;

	HealthController health;
	void Awake () {
		health = GetComponent<HealthController>();
	}

	void OnEnable () {
		health.onDeath += DropItem;
	}

	void OnDisable () {
		health.onDeath -= DropItem;
	}

	void DropItem () {
		if (Random.value > dropChance) return;

		string item = prefabNames[Random.Range(0, prefabNames.Length)];
		GameObject g = Spawner.Spawn(item);
		Bounds b = gameObject.RendererBounds();
		g.transform.position = b.center;		
	}
}
