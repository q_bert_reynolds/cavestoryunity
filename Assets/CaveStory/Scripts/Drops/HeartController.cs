﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Paraphernalia.Components;

public class HeartController : MonoBehaviour {

	public int id = 0;
	public int health = 2;
	public float delay = 4;
	public float flashDuration = 4;

	public Sprite circleSprite;

	private SpriteRenderer sprite;

	void Awake () {
		sprite = GetComponent<SpriteRenderer>();
	}

	void OnEnable () {
		StartCoroutine("Disappear");
	}

	void OnDisable () {
		StopCoroutine("Disappear");
		sprite.enabled = true;
	}

	void OnTriggerEnter2D (Collider2D c) {
		if (c.gameObject.tag == "Player") {
			AudioManager.PlayEffect("health");
			HealthController hc = PlayerController.instance.GetComponent<HealthController>();
			hc.health += this.health;
			gameObject.SetActive(false);
		}
	}

	IEnumerator Disappear () {
		yield return new WaitForSeconds(delay);
		for (float t = 0; t < flashDuration; t += Time.deltaTime) {
			sprite.enabled = !sprite.enabled;
			yield return new WaitForEndOfFrame();
		}
		SequenceAnimator c = GetComponent<SequenceAnimator>();
		c.enabled = false;
		sprite.sprite = circleSprite;
		yield return new WaitForSeconds(0.05f);
		gameObject.SetActive(false);
		c.enabled = true;
	}
}
