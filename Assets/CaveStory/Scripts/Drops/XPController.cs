﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Paraphernalia.Components;

public class XPController : MonoBehaviour {

	public int points = 1;
	public float bounceTime = 6;
	public float flashTime = 2;

	private SpriteRenderer sprite;

	void Awake () {
		sprite = GetComponent<SpriteRenderer>();
	}

	void OnEnable () {
		StartCoroutine("Disappear");

		Rigidbody2D body = GetComponent<Rigidbody2D>();
		if (body != null) {
			body.velocity = Vector2.up * Mathf.Sqrt(2 * Physics2D.gravity.magnitude * body.gravityScale) + Random.insideUnitCircle * 3;
		}
	}

	void OnDisable () {
		StopCoroutine("Disappear");
		sprite.enabled = true;
	}

	void OnCollisionEnter2D (Collision2D c) {
		if (c.gameObject.tag == "Player") {
			AudioManager.PlayEffect("gainXP");
			PlayerController.currentGun.AddXP(points);
			gameObject.SetActive(false);
		}
		else {
			AudioManager.PlayEffect("bounceXP");
		}
	}

	IEnumerator Disappear () {
		yield return new WaitForSeconds(bounceTime);
		for (float t = 0; t < flashTime; t += Time.deltaTime) {
			sprite.enabled = !sprite.enabled;
			yield return new WaitForEndOfFrame();
		}
		gameObject.SetActive(false);
	}
}
