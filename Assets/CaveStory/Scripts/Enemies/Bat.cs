﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Paraphernalia.Utils;

public class Bat : MonoBehaviour {

	public float maxHeight = 4;
	public float speed = 2;

	private Vector3 startPosition;
	private Vector3 endPosition;

	void Start () {
		startPosition = transform.position;
		endPosition = startPosition + Vector3.up * maxHeight;
		StartCoroutine("Fly");
	}

	void Update () {
		if (PlayerController.instance == null) return;
		Vector3 p = PlayerController.instance.transform.position;
		if (p.x > transform.position.x) transform.right = Vector3.right;
		else transform.right = Vector3.left;
	}

	IEnumerator Fly () {
		transform.position = Vector3.Lerp(startPosition, endPosition, Random.value);
		while (enabled) {
			float duration = maxHeight / speed;
			for (float t = 0; t < duration; t += Time.deltaTime) {
				float frac = Interpolate.Ease(Interpolate.EaseType.InOutSine, t / duration);
				transform.position = Vector3.Lerp(startPosition, endPosition, frac);
				yield return new WaitForEndOfFrame();
			}
			for (float t = 0; t < duration; t += Time.deltaTime) {
				float frac = Interpolate.Ease(Interpolate.EaseType.InOutSine, t / duration);
				transform.position = Vector3.Lerp(endPosition, startPosition, frac);
				yield return new WaitForEndOfFrame();
			}
		}
	}

	void OnDrawGizmos () {
		Gizmos.color = new Color(1,1,0.5f,0.5f);
		Gizmos.DrawLine(transform.position, transform.position + Vector3.up * maxHeight);
	}
}
