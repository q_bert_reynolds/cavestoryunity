﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Paraphernalia.Components;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class Critter : MonoBehaviour {

	public float alertDistance = 6;
	public float attackDistance = 3;
	public float jumpDelay = 1;

	private Animator anim;
	private JumpController jump;
	private HealthController health;
	private float jumpTime; 

	void Awake () {
		anim = GetComponent<Animator>();
		jump = GetComponent<JumpController>();
		health = GetComponent<HealthController>();
	}

	void OnEnable () {
		health.onHealthChanged += OnHealthChanged;
	}

	void OnDisable () {
		health.onHealthChanged -= OnHealthChanged;
	}

	void Start () {
		jumpTime = Time.time - jumpDelay;
	}

	void OnHealthChanged (float health, float prevHealth, float maxHealth) {
		if (health < prevHealth) {
			anim.SetTrigger("hurt");
			jumpTime = Time.time;
		}
	}

	void Update () {
		if (PlayerController.instance == null) return;
		Vector3 p = PlayerController.instance.transform.position - transform.position;
		transform.right = Vector3.right * p.x;

		if (Mathf.Abs(p.x) < attackDistance
			&& Mathf.Abs(p.y) < alertDistance
			&& Time.time - jumpTime > jumpDelay
			&& jump.Jump(Mathf.Sign(p.x))) {

			jumpTime = Time.time;
			AudioManager.PlayEffect("critterJump");
		}

		anim.SetBool("alert", Mathf.Abs(p.x) < alertDistance);
 		anim.SetBool("onGround", jump.onGround);
	}

	#if UNITY_EDITOR
	void OnDrawGizmos () {
		Handles.matrix = transform.localToWorldMatrix;
		Handles.color = new Color(1,1,0,0.1f);
		Handles.DrawWireCube(Vector3.zero, new Vector3(alertDistance, alertDistance, 0));
		Handles.color = new Color(1,0,0,0.3f);
		Handles.DrawWireCube(Vector3.zero, new Vector3(attackDistance, alertDistance, 0));
	}
	#endif
}
