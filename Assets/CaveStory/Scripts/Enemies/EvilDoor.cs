﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EvilDoor : MonoBehaviour {


	Animator anim;
	HealthController health;

	void Awake () {
		health = GetComponent<HealthController>();
		anim = GetComponent<Animator>();
	}

	void OnEnable () {
		health.onDeath += OnDeath;
	}

	void OnDisable () {
		health.onDeath -= OnDeath;
	}

	void Update () {
		if (PlayerController.instance == null) return;
		Vector3 p = PlayerController.instance.transform.position;
		float d = Mathf.Abs(p.x - transform.position.x);
		anim.SetFloat("distance", d);
		anim.SetBool("isAbove", p.y > transform.position.y);
	}

	void OnDeath () {
		PlayerController.AddFlag(301);
	}
}
