﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gravekeeper : MonoBehaviour {

	public float speed = 1;
	public float activeDistance = 6;

	public enum State {
		Waiting,
		Chasing,
		Attacking
	}
	State state = State.Waiting;

	HealthController health;
	Animator anim;
	Rigidbody2D body;
	SpriteRenderer sprite;

	void Awake () {
		health = GetComponent<HealthController>();
		anim = GetComponent<Animator>();
		body = GetComponent<Rigidbody2D>();
		sprite = GetComponentInChildren<SpriteRenderer>();
	}

	void OnEnable () {
		health.onHealthChanged += OnHealthChanged;
	}

	void OnDisable () {
		health.onHealthChanged -= OnHealthChanged;
	}

	void OnHealthChanged (float health, float prevHealth, float maxHealth) {
		if (health < prevHealth) {
			StopCoroutine("Shake");
			StartCoroutine("Shake");
		}
	}

	IEnumerator Shake () {
		for (int i = 0; i < 3; i++) {
			sprite.transform.localPosition = Vector3.right * 0.1f;
			yield return new WaitForSeconds(0.1f);
			sprite.transform.localPosition = Vector3.left * 0.1f;
			yield return new WaitForSeconds(0.1f);
		}
		sprite.transform.localPosition = Vector3.zero;
	}

	void Update () {
		Vector3 d = PlayerController.instance.transform.position - transform.position;
		switch (state) {
			case State.Waiting:
				transform.right = Vector3.right * Mathf.Sign(d.x);
				body.velocity = Vector2.zero;
				if (d.magnitude < activeDistance) {
					state = State.Chasing; 
					anim.SetTrigger("chase");
				}
				break;
			case State.Chasing:
				transform.right = Vector3.right * Mathf.Sign(d.x);
				body.velocity = transform.right * speed;
				if (Mathf.Abs(d.x) <= 1) {
					state = State.Attacking;
					anim.SetTrigger("attack");
				}
				break;
			default:
				break;
		}
	}

	void AttackFinished () {
		state = State.Waiting;
		anim.SetTrigger("wait");
	}
}
