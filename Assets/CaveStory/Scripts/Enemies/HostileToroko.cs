﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HostileToroko : MonoBehaviour {

	HealthController health;
	Animator anim;

	void Awake () {
		health = GetComponent<HealthController>();
		anim = GetComponent<Animator>();
	}

	void OnTriggerEnter2D (Collider2D c) {
		if (LayerMask.LayerToName(c.gameObject.layer) != "PlayerBullet") return;
		anim.Play("takeHit");
	}
}
