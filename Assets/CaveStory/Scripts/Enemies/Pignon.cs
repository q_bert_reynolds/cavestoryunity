﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pignon : MonoBehaviour {

	public float speed = 1;
	float targetVelocity;

	HealthController health;
	Animator anim;
	Rigidbody2D body;

	void Awake () {
		health = GetComponent<HealthController>();
		anim = GetComponent<Animator>();
		body = GetComponent<Rigidbody2D>();
	}

	void OnEnable () {
		StartCoroutine("UpdateCoroutine");
		health.onHealthChanged += OnHealthChanged;
	}

	void OnDisable () {
		health.onHealthChanged -= OnHealthChanged;
	}

	void OnHealthChanged (float health, float prevHealth, float maxHealth) {
		if (health < prevHealth) {
			anim.SetTrigger("takeHit");
		}
	}

	IEnumerator UpdateCoroutine () {
		if (Random.value > 0.5f) transform.right = Vector3.left;
		yield return new WaitForSeconds(Random.value);
		while (enabled) {
			switch (Random.Range(0,3)) {
				case 0:
					targetVelocity = 0;
					break;
				case 1:
					targetVelocity = transform.right.x * speed;
					break;
				default:
					transform.right = -transform.right;
					targetVelocity = 0;
					break;
			}
			yield return new WaitForSeconds(Random.value);
		}
	}

	void FixedUpdate () {
		body.velocity = new Vector2(targetVelocity, body.velocity.y);
		anim.SetFloat("speed", Mathf.Abs(body.velocity.x));
	}

	void OnCollisionEnter2D (Collision2D c) {
		foreach (ContactPoint2D cp in c.contacts) {
			float ang = Vector2.Angle(cp.normal, Vector2.up);
			if (ang > 85 && ang < 90) {
				transform.right = -transform.right;
			}
		}
	}
}
