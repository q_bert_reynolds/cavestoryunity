﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoEvent : MonoBehaviour {

	public string mapName;
	public int eventID;

	void Start () {
		TSCParser.RunEvent(mapName, eventID);
	}
}
