﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerEvent : Interactable {

	void OnTriggerEnter2D (Collider2D c) {
		if (c.gameObject.tag == "Player") {
			Interact();
		}
	}
}
