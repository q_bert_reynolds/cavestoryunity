﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlagState : MonoBehaviour {

	public int[] flags;
	public bool enable;

	void Awake () {
		bool hasAny = false;
		foreach (int flag in flags) {
			if (PlayerController.HasFlag(flag)) {
				hasAny = true;
				break;
			}
		}
		if (hasAny != enable) gameObject.SetActive(false);
	}
}
