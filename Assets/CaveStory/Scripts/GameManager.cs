﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Paraphernalia.Components;

public class GameManager : MonoBehaviour {

	public static GameManager instance;
	public AudioClip[] music;
	public Gun[] guns;
	public Item[] items;
	public Sprite[] faces;
	public Sprite plusSign;
	public Sprite minusSign;
	public Sprite[] positiveNumbers;
	public Sprite[] negativeNumbers;
	public AnimatableCharacter[] npcs;

	public List<string> destroyedNPCs = new List<string>();

	void Awake () {
		if (instance == null) {
			instance = this;
		}
	}
	void OnEnable () {
		HealthController.onAnyDeath += OnAnyDeath;
	}

	void OnDisable () {
		HealthController.onAnyDeath -= OnAnyDeath;
	}

	void OnAnyDeath (HealthController controller) {
		if (controller.gameObject.tag == "Player") {
			StartCoroutine("PlayerDeath");
		}
	}

	IEnumerator PlayerDeath () {
		AudioManager.CrossfadeMusic(music[3], 0.1f, false);
		yield return new WaitForSecondsRealtime(3);
		TextController.ShowText("\nYou have died.");
		TextController.AskYesNo("Want to retry?", RestartOrQuitToMenu);
	}

	public static void RestartOrQuitToMenu (bool restart) {
		if (restart) SavePoint.Load();
		else QuitToMenu();
	}

	public static void QuitToMenu () {
		DestroyImmediate(PlayerController.instance.gameObject);
		SceneManager.LoadScene("MainMenu");
	}

	public static void LoadScene (string scene, int doorID, TransitionGraphic.Style fadeOutStyle, TransitionGraphic.Style fadeInStyle) {
		instance.StartCoroutine(instance.LoadSceneCoroutine(scene, doorID, fadeOutStyle, fadeInStyle));
	}

	IEnumerator LoadSceneCoroutine (string scene, int doorID, TransitionGraphic.Style fadeOutStyle, TransitionGraphic.Style fadeInStyle) {
		Time.timeScale = 0;
		yield return TransitionGraphic.FadeOut(fadeOutStyle);
		string lastScene = SceneManager.GetActiveScene().name;
		yield return SceneManager.LoadSceneAsync(scene);
		LocationText.DoorOpened(TSCParser.GetMapName(scene));
		Door[] doors = GameObject.FindObjectsOfType<Door>();
		foreach (Door door in doors) {
			if (lastScene == door.sceneName && doorID == door.doorID) {
				PlayerController.instance.transform.position = door.transform.position;
				break;
			}
		}
		if (CameraController.instance != null) {
			CameraController.SetOffset(Vector3.forward * CameraController.instance.offset.z);
		}
		yield return TransitionGraphic.FadeIn(fadeInStyle);
		Time.timeScale = 1;
	}

	public static Sprite GetNumberSprite (int n, int sign = 1) {
		if (instance == null) return null;
		
		n = Mathf.Clamp(n, -9, 9);
		if (n < 0) return instance.negativeNumbers[-n];
		else if (n > 0) return instance.positiveNumbers[n];
		
		if (n < 0 || sign < 0) return instance.negativeNumbers[0];
		else return instance.positiveNumbers[0];
	}

	public static Sprite GetSignSprite (int n) {
		if (n < 0) return instance.minusSign;
		else return instance.plusSign;
	}

	public static Item GetItem (int id) {
		foreach (Item item in instance.items) {
			if (item.id == id) {
				return item;
			}
		}
		return null;
	}

	public static Gun GetGun (int id) {
		foreach (Gun gun in instance.guns) {
			if (gun.id == id) {
				return gun;
			}
		}
		return null;
	}

	public static Sprite GetItemIcon (int id) {
		if (id >= 1000) {
			foreach (Item i in instance.items) {
				if (i.id == id - 1000) return i.icon;
			}
		}
		else {
			foreach (Gun g in instance.guns) {
				if (g.id == id) return g.icon;
			}
		}

		return null;
	}

	public static Sprite GetFace (int id) {
		return instance.faces[id];
	}

	public static Interactable FindNPC (int id) {
		Interactable[] npcs = GameObject.FindObjectsOfType(typeof(Interactable)) as Interactable[];
		foreach (Interactable npc in npcs) {
			if (npc.eventID == id) return npc;
		}
		Debug.Log("NPC " + id + " not found");
		return null;
	}

	public static AnimatableCharacter FindAnimatableNPC (int id) {
		Interactable npc = FindNPC(id);
		if (npc != null) return npc.GetComponent<AnimatableCharacter>();
		return null;
	}

	public static AnimatableCharacter InstantiateNPC (int type) {
		if (type == 0) return null;
		foreach (AnimatableCharacter npc in instance.npcs) {
			if (npc.type == type) return GameObject.Instantiate(npc) as AnimatableCharacter;
		}
		Debug.Log("NPC of type " + type + " not found");
		return null;
	}

	public static void DestroyNPC (int id) {
		Interactable npcToDestroy = GameManager.FindNPC(id);
		if (npcToDestroy != null) GameObject.Destroy(npcToDestroy.gameObject);
	}
}
