﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Paraphernalia.Components;

public class Door : Interactable {

	[Tooltip("Used to place player at door with matching doorID.")] public int doorID = 0;
	public string sceneName;
	public string audioClipName = "openDoor";
	public Sprite openSprite;
	public TransitionGraphic.Style transitionStyleA;
	public TransitionGraphic.Style transitionStyleB;
	private SpriteRenderer sprite;
	
	void Awake () {
		sprite = GetComponent<SpriteRenderer>();
	}
	public override void Interact () {
		if (eventID > 0) {
			base.Interact();
			return;
		}
		
		if (openSprite != null) sprite.sprite = openSprite;
		AudioManager.PlayEffect(audioClipName);
		GameManager.LoadScene(sceneName, doorID, transitionStyleA, transitionStyleB);
	}
}
