﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Paraphernalia.Components;

public class HealPoint : Interactable {

	public override void Interact () {
		AudioManager.PlayEffect("health");
		HealthController health = PlayerController.instance.GetComponent<HealthController>();
		health.health += health.maxHealth;
		TextController.ShowText("Health refilled.");

		//TODO: missiles
	}
}
