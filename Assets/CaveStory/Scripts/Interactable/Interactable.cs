﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Interactable : MonoBehaviour {

	public int eventID;
	
	public virtual void Interact () {
		StartCoroutine(Interaction());
	}

	IEnumerator Interaction () {
		string mapName = SceneManager.GetActiveScene().name;
		yield return TSCParser.RunEvent(mapName, eventID);
	}

	public void SetDirection (int dir) {
		switch (dir) {
			case 0: // Left
				transform.right = -Vector3.right;
				break;
			case 1: // Up
				break;
			case 2: // Right
				transform.right = Vector3.right;
				break;
			case 3: // Down
				break;
			case 4: // Center
				Debug.Log("face center?");
				break;
			default:
				Debug.LogWarning("Unknown direction: " + dir);
				break;
		}
	}
}
