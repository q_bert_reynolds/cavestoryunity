﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeCapsuleController : Interactable {

	public Sprite itemSprite;
	public AudioClip music;
	public int maxLifeIncrease = 3;

	void Start () {
		if (PlayerController.instance.flags.Contains(eventID)) {
			gameObject.SetActive(false);
		}
	}

	public override void Interact () {
		TextController.DisplayItem(
			itemSprite, 
			new string[] {
				"Got a ●Life Capsule●!", 
				"Max health increased by " + maxLifeIncrease + "!"
			}, 
			IncreaseHealth,
			null,
			music
		);
	}

	void IncreaseHealth () {
		PlayerController player = PlayerController.instance;
		player.GetComponent<HealthController>().maxHealth += maxLifeIncrease;		
		gameObject.SetActive(false);
		player.flags.Add(eventID);
	}
}
