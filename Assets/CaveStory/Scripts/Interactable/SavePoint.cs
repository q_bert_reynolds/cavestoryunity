﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
using System;

public class SavePoint : Interactable {

	[System.Serializable]
	public class SaveState {
		public string scene;
		public Vector3 position;
		public int[] gunXPs;
		public int[] flags;
		public string[] destroyed;
		public float health;
		public float maxHealth;
	}

	public override void Interact () {
		TextController.AskYesNo("Do you want to save?", SaveGameCallback);
	}

	void SaveGameCallback (bool save) {
		if (save) {
			Save();
			TextController.ShowText("Game saved.");
		}
	}

	public static void Save () {
		SaveState saveState = new SaveState();
		saveState.scene = SceneManager.GetActiveScene().name;
		
		PlayerController player = PlayerController.instance;
		saveState.position = player.transform.position;

		Gun[] guns = GameManager.instance.guns;
		saveState.gunXPs = new int[guns.Length];
		for (int i = 0; i < guns.Length; i++) {
			if (PlayerController.HasGun(guns[i])) saveState.gunXPs[i] = guns[i].xp;
			else saveState.gunXPs[i] = -1;
		}
		saveState.flags = player.flags.ToArray();

		HealthController health = player.GetComponent<HealthController>();
		saveState.health = health.health;
		saveState.maxHealth = health.maxHealth;

		string path = Path.Combine(Application.persistentDataPath, "save.json");
		string json = JsonUtility.ToJson(saveState);

		File.WriteAllText(path, json);

		Debug.Log(path);
	}

	public static SaveState currentSaveState;
	public static void Load () {
		if (TransitionGraphic.instance != null) TransitionGraphic.instance.state = TransitionGraphic.State.Off;
		if (TextController.instance != null) TextController.Close();

		string path = Path.Combine(Application.persistentDataPath, "save.json");
		if (!File.Exists(path)) {
			if (PlayerController.instance != null) {
				DestroyImmediate(PlayerController.instance.gameObject);
			}
			SceneManager.LoadScene("Start Point");
			return;
		}
		string json = File.ReadAllText(path);
		currentSaveState = JsonUtility.FromJson<SaveState>(json);
		SceneManager.sceneLoaded += FinishLoading;
		SceneManager.LoadScene(currentSaveState.scene);
	}

	static void FinishLoading (Scene s, LoadSceneMode l) {
		SceneManager.sceneLoaded -= FinishLoading;
		PlayerController player = PlayerController.instance;
		player.transform.position = currentSaveState.position;
		
		Gun[] guns = GameManager.instance.guns;
		PlayerController.instance.guns.Clear();
		for (int i = 0; i < guns.Length; i++) {
			if (currentSaveState.gunXPs[i] >= 0) {
				Gun gun = GameManager.instance.guns[i];
				gun.xp = currentSaveState.gunXPs[i];
				PlayerController.instance.guns.Add(gun);
			}
		}
		player.SetupGun();
		
		player.flags = new List<int>(currentSaveState.flags);

		HealthController health = player.GetComponent<HealthController>();
		health.health = currentSaveState.health;
		health.maxHealth = currentSaveState.maxHealth;

		player.gameObject.SetActive(true);
	}
}
