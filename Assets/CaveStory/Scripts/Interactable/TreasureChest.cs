﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using Paraphernalia.Components;

public class TreasureChest : Interactable {

	public int flag;
	public Sprite[] sprites;

	SpriteRenderer spriteRenderer;

	void Awake () {
		spriteRenderer = GetComponent<SpriteRenderer>();
	}
	
	void Start () {
		if (PlayerController.HasFlag(flag)) spriteRenderer.sprite = sprites[3];
		else StartCoroutine("Glint");
	}

	IEnumerator Glint () {
		while (enabled) {
			spriteRenderer.sprite = sprites[0];
			yield return new WaitForSeconds(Random.Range(1,3));
			spriteRenderer.sprite = sprites[1];
			yield return new WaitForSeconds(0.1f);
			spriteRenderer.sprite = sprites[2];
			yield return new WaitForSeconds(0.1f);
		}
	}

	// public override void ChangeSprite (int id) {
	// 	StopCoroutine("Glint");
	// 	spriteRenderer.sprite = sprites[3];
	// }
}
