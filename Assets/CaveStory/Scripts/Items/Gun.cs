﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : Item {

	public delegate void OnGunChanged(Gun gun);
	public static event OnGunChanged onXPChanged = delegate {};
	public static event OnGunChanged onLevelChanged = delegate {};
	
	public int[] xpPerLevel;
	public string[] bulletPrefabNames;
	public int xp;
	private int _prevXP;
	public int prevXP { get { return _prevXP; }}
	private int _prevLevel;
	public int prevLevel { get { return _prevLevel; }}
	public int maxAmmo;
	public int currentAmmo;
	
	void Awake () {
		_prevXP = xp;
		_prevLevel = currentLevel;
	}

	public string bulletPrefabName {
		get { return bulletPrefabNames[currentLevel-1]; }
	}

	public int levels {
		get { return xpPerLevel.Length; }
	}

	public int maxXP {
		get { return xpPerLevel[levels-1]; }
	}

	public int currentLevel {
		get {
			for (int i = 0; i < levels; i++) {
				if (xp < xpPerLevel[i]) return i + 1;
			}
			return levels;
		}
	}

	public float progress {
		get {
			float minXP = 0;
			for (int i = 0; i < levels; i++) {
				float maxXP = xpPerLevel[i];
				if (xp < maxXP) return (xp - minXP) / (maxXP - minXP);
				else minXP = maxXP;
			}	
			return 1;		
		}
	}

	public void AddXP (int points) {
		_prevLevel = currentLevel;
		_prevXP = xp;
		xp = Mathf.Clamp(xp + points, 0, maxXP);
		onXPChanged(this);
		if (prevLevel != currentLevel) {
			onLevelChanged(this);
		}
	}
}
