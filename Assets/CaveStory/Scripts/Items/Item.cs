﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : ScriptableObject {

	public int id;
	public Sprite icon;
	public string displayName;
	[Multiline] public string description;
}
