﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpController : MonoBehaviour {

	[Range(1, 90)] public float maxGroundAngle = 50;
	public LayerMask environmentMask;
	public float jumpGravityScale = 0.7f;
	public float jumpHeight = 3;

	Collider2D groundCollider;
	Rigidbody2D body;
	ContactPoint2D[] contacts = new ContactPoint2D[10];
	bool _onGround;
	public bool onGround { get { return _onGround; } }

	void Awake () {
		body = GetComponent<Rigidbody2D>();
		groundCollider = GetComponent<Collider2D>();
	}

	public bool Jump (float x) {
		return Jump(new Vector2(x, jumpHeight));
	}

	public bool Jump (Vector2 d) {
		if (onGround) {
			body.velocity = new Vector2(d.x, JumpVelocity(d.y));
			return true;
		}
		return false;
	}

	public float JumpVelocity () {
		return JumpVelocity(jumpHeight);
	}

	public float JumpVelocity (float height) {
		return Mathf.Sqrt(2 * height * Physics2D.gravity.magnitude * body.gravityScale);
	}

	private float friction {
		get { 
			if (groundCollider.sharedMaterial != null) return groundCollider.sharedMaterial.friction;
			return 0;
		}
		set {
			if (groundCollider.sharedMaterial != null) groundCollider.sharedMaterial.friction = value;
		}
	}

	void GroundCheck () {
		float prevFriction = friction;
		_onGround = false;
		friction = 1;
		int contactCount = groundCollider.GetContacts(contacts);
		for (int j = 0; j < contactCount; j++) {
			float ang = Vector2.Angle(contacts[j].normal, Vector2.up);
			if (ang < maxGroundAngle) _onGround = true;
			else if (ang < 180 - maxGroundAngle) friction = 0;
		}	
		
		if (prevFriction != friction) {
			groundCollider.enabled = false;
			groundCollider.enabled = true;	
		}
	}

	void OnCollisionEnter2D (Collision2D c) {
		body.velocity = lastVelociy;
	}

	Vector2 lastVelociy;
	void FixedUpdate () {
		lastVelociy = body.velocity;
		GroundCheck();
	}
}
