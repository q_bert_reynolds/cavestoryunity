﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Paraphernalia.Extensions;
using Paraphernalia.Components;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Animator))]
public class PlayerController : MonoBehaviour {

	private static PlayerController _instance;
	public static PlayerController instance {
		get {
			if (_instance == null) {
				_instance = FindObjectOfType(typeof(PlayerController)) as PlayerController;
			}
			return _instance;
		}
	}

	public delegate void OnGunChanged (Gun gun);
	public static event OnGunChanged onGunChanged = delegate {};

	public List<int> flags = new List<int>();
	public List<Item> items = new List<Item>();
	public List<Gun> guns = new List<Gun>();
	private int gunIndex = 0;
	public static Gun currentGun {
		get {
			if (instance == null || instance.guns.Count == 0) return null;
			instance.gunIndex %= instance.guns.Count;
			return instance.guns[instance.gunIndex];
		}
	}
	public float speed = 4;
	public float invincibilityDuration = 2;
	public LayerMask interactableMask;
	public bool inWater { get; set; }
	public SpriteRenderer gunSprite;

	private bool _inputLocked = false;
	public bool inputLocked {
		get { return _inputLocked; }
		set {
			if (_inputLocked != value) {
				_inputLocked = value;
				if (_inputLocked) {
					body.gravityScale = 1;
					
					Vector2 v = body.velocity;
					v.x = 0;
					body.velocity = v;

					CameraController cam = CameraController.instance;
					if (cam != null && cam.target == transform) {
						cam.offset = new Vector3(0, 0, cam.offset.z);
					}

					anim.SetFloat("speed", 0);
				}
			}
		}
	}

	Rigidbody2D body;
	Animator anim;
	SpriteRenderer[] sprites;
	ProjectileLauncher gun;
	HealthController health;
	JumpController jump;

	Vector2 targetVelocity;
	bool interacting = false;

	void Interact () {
		if (interacting) return;
		interacting = true;
		anim.SetBool("interacting", true);

		Collider2D c = Physics2D.OverlapCircle(transform.position, 0.2f, interactableMask);
		if (c == null) {
			GameObject g = Spawner.Spawn("Question", false);
			g.transform.position = transform.position + Vector3.up * 0.5f;
			g.SetActive(true);
			return;
		}

		Interactable interactable = c.gameObject.GetComponent<Interactable>();
		if (interactable) interactable.Interact();
	}

	void StopInteracting () {
		interacting = false;
		anim.SetBool("interacting", false);
	}

	public static void ReceiveItem (int id) {
		ReceiveItem(GameManager.GetItem(id));
	}

	public static void ReceiveItem (Item item) {
		if (item == null || instance.items.Contains(item)) return;
		instance.items.Add(item);
	}

	public static void LoseItem (int id) {
		foreach (Item item in instance.items) {
			if (item.id == id) {
				instance.items.Remove(item);
				return;
			}
		}
	}

	public static bool HasItem (int id) {
		foreach (Item item in instance.items) {
			if (item.id == id) {
				return true;
			}
		}
		return false;
	}
	
	public static bool HasItem (Item item) {
		return instance.items.Contains(item);
	}

	public static void ReceiveGun (int id) {
		ReceiveGun(GameManager.GetGun(id));
	}

	public static void ReceiveGun (Gun gun) {
		if (gun == null || HasGun(gun)) return;

		gun.xp = 0;
		instance.guns.Add(gun);
		instance.SetupGun();
		onGunChanged(gun);
	}

	public static void LoseGun (int id) {
		foreach (Gun gun in instance.guns) {
			if (gun.id == id) {
				instance.guns.Remove(gun);
				return;
			}
		}
	}

	public static bool HasGun (int id) {
		foreach (Gun gun in instance.guns) {
			if (gun.id == id) {
				return true;
			}
		}
		return false;
	}

	public static bool HasGun (Gun gun) {
		return instance.guns.Contains(gun);
	}

	public void SetupGun () {
		if (currentGun) {
			gunSprite.gameObject.SetActive(true);
			gun.projectileName = currentGun.bulletPrefabName;
		}
		else {
			gunSprite.gameObject.SetActive(false);
		}
		onGunChanged(currentGun);
	}

	void OnLevelChanged (Gun gunData) {
		gun.projectileName = gunData.bulletPrefabName;
		string s = (gunData.currentLevel > gunData.prevLevel) ? "Up" : "Down";
		AudioManager.PlayEffect("level" + s);
		GameObject g = Spawner.Spawn("Level" + s, false);
		g.transform.position = transform.position;
		g.SetActive(true);
	}

	void OnHealthChanged (float health, float prevHealth, float maxHealth) {
		if (health < prevHealth && health > 0) {
			if (currentGun) currentGun.AddXP((int)(health - prevHealth) * 2);
			StartCoroutine("TemporaryInvincibility");
		}
	}

	IEnumerator TemporaryInvincibility () {
		health.enabled = false;
		for (float t = 0; t < invincibilityDuration; t += Time.deltaTime) {
			foreach (SpriteRenderer sprite in sprites) sprite.enabled = !sprite.enabled;
			yield return new WaitForEndOfFrame();
			yield return new WaitForEndOfFrame();
		}
		foreach (SpriteRenderer sprite in sprites) sprite.enabled = true;
		health.enabled = true;
	}

	public static bool HasFlag (int flag) {
		return instance.flags.Contains(flag);
	}

	public static void AddFlag (int flag) {
		if (!HasFlag(flag)) instance.flags.Add(flag);
	}

	public static void RemoveFlag (int flag) {
		instance.flags.Remove(flag);
	}

	void Awake () {
		if (_instance == null) _instance = this;
		else if (_instance != this) Destroy(gameObject);

		body = GetComponent<Rigidbody2D>();
		anim = GetComponent<Animator>();
		gun = GetComponentInChildren<ProjectileLauncher>();
		health = GetComponent<HealthController>();
		jump = GetComponent<JumpController>();
		sprites = GetComponentsInChildren<SpriteRenderer>();
		DontDestroyOnLoad(gameObject);
	}

	void OnEnable () {
		health.onHealthChanged += OnHealthChanged;
		Gun.onLevelChanged += OnLevelChanged;
	}

	void OnDisable () {
		if (health != null) health.onHealthChanged -= OnHealthChanged;
		Gun.onLevelChanged -= OnLevelChanged;
	}

	void Start () {
		SetupGun();
	}
	
	void Update () {
		Vector2 v = body.velocity;
		
		if (Time.timeScale < 0.1f || inputLocked) return;

		anim.SetBool("onGround", jump.onGround);

		float x = Input.GetAxis("Horizontal");
		float y = Mathf.Round(Input.GetAxisRaw("Vertical"));
		anim.SetFloat("yInput", y);

		CameraController cam = CameraController.instance;
		if (cam != null && cam.target == transform) {
			cam.offset = new Vector3(transform.right.x * 4, y * 4, cam.offset.z);
		}

		if (x > 0) transform.right = Vector3.right;
		else if (x < 0) transform.right = -Vector3.right;

		float s = inWater ? speed * 0.5f : speed;
		targetVelocity = new Vector2(x * s, v.y);
		if (Input.GetButtonDown("Jump") && jump.onGround) {
			body.gravityScale = jump.jumpGravityScale;
			float h = inWater ? 1 : jump.jumpHeight;
			anim.SetTrigger("jump");
			AudioManager.PlayEffect("jump");
			targetVelocity.y = jump.JumpVelocity(h);
			body.velocity = targetVelocity;
		}
		else if (!Input.GetButton("Jump") && !jump.onGround) {
			targetVelocity.y = Mathf.Min(targetVelocity.y, jump.JumpVelocity(1));
		}

		anim.SetFloat("speed", v.magnitude);
		anim.SetFloat("yVel", v.y);
		bool shouldShoot = Input.GetButtonDown("Fire");
		if (shouldShoot) gun.Shoot(gun.transform.right);

		if (Mathf.Abs(x) < 0.5f && y < -0.5f && jump.onGround) Interact();
		else if (Mathf.Abs(x) > 0.5f || y > -0.5f || shouldShoot || !jump.onGround) StopInteracting();
	}

	void FixedUpdate () {
		if (inputLocked) return;
		Vector3 change = targetVelocity - body.velocity;
		body.AddForce(change, ForceMode.VelocityChange);
		if (body.velocity.y <= 0) body.gravityScale = 1;			
	}

	void OnCollisionEnter2D (Collision2D c) {
		// if (!c.gameObject.InLayerMask(jump.environmentMask)) return;
		foreach (ContactPoint2D cp in c.contacts) {
			if (Vector2.Angle(cp.normal, Vector2.up) < jump.maxGroundAngle && c.relativeVelocity.y > 7) {
				AudioManager.PlayEffect("land");
			}
			else if (Vector2.Angle(cp.normal, Vector2.down) < jump.maxGroundAngle) {
				AudioManager.PlayEffect("bumpHead");
				ParticleManager.Play("BumpHead", cp.point);
			}
		}
	}

	public void Step () {
		AudioManager.PlayEffect("step");
	}

	public static void SetDirection (int dir) {
		switch (dir) {
			case 0: // Left
				instance.anim.SetFloat("yInput", 0);
				instance.transform.right = -Vector3.right;
				break;
			case 1: // Up
				instance.anim.SetFloat("yInput", 1);
				break;
			case 2: // Right
				instance.anim.SetFloat("yInput", 0);
				instance.transform.right = Vector3.right;
				break;
			case 3: // Down
				instance.anim.SetFloat("yInput", -1);
				break;
			default:
				Debug.LogWarning("Unknown direction: " + dir);
				break;
		}
	}

	public static void SetMapPosition (Vector2 pos) {
		pos = new Vector2(pos.x + 0.5f, -pos.y - 1);
		instance.transform.position = pos;
	}
	
	public static void Bump (int dir) {
		switch (dir) {
			case 0:
				instance.jump.Jump(new Vector2(2, 0.75f));
				break;
			case 2:
				instance.jump.Jump(new Vector2(-2, 0.75f));
				break;
			default:
				instance.jump.Jump(new Vector2(0, 0.75f));
				break;
		}			
	}
}
