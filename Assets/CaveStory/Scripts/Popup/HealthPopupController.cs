﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(HealthController))]
public class HealthPopupController : MonoBehaviour {

	public Vector3 offset;
	public float delay = 0.5f;

	HealthController health;
	int diff;

	void Awake () {
		health = GetComponent<HealthController>();
		health.onHealthChanged += OnHealthChanged;
	}

	void OnEnable () {
		SceneManager.sceneLoaded += OnSceneLoad;
	}

	void OnDisable () {
		SceneManager.sceneLoaded -= OnSceneLoad;
	}

	void OnSceneLoad (Scene scene, LoadSceneMode mode) {
		diff = 0;
	}

	void OnDestroy () {
		health.onHealthChanged -= OnHealthChanged;
	}

	void OnHealthChanged(float health, float prevHealth, float maxHealth) {
		int d = (int)(health - prevHealth);
		if (d < 0) {
			diff += d;
			StopCoroutine("DelayPopup");
			if (health > 0) StartCoroutine("DelayPopup");
			else PopupController.PopupNumber(diff, transform, offset, false);
		}
	}

	IEnumerator DelayPopup () {
		yield return new WaitForSeconds(delay);
		PopupController.PopupNumber(diff, transform, offset, true);
		diff = 0;
	}
}
