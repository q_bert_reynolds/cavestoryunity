﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupController : MonoBehaviour {

	public float showDuration = 0.5f;
	public float waitDuration = 1;
	public float hideDuration = 0.5f;
	public float height = 1;

	Image[] images;

	void Awake () {
		images = GetComponentsInChildren<Image>();
	}

	void OnEnable () {
		StartCoroutine("PopupCoroutine");
	}

	IEnumerator PopupCoroutine() {
		foreach (Image i in images) {
			i.fillAmount = 1;
			i.fillOrigin = (int)Image.OriginVertical.Bottom;
		}

		Vector3 startPos = transform.localPosition;
		Vector3 endPos = startPos + Vector3.up * height;
		for (float t = 0; t < showDuration; t += Time.deltaTime) {
			float frac = t / showDuration;
			transform.localPosition = Vector3.Lerp(startPos, endPos, frac);
			yield return new WaitForEndOfFrame();
		}
		
		yield return new WaitForSeconds(waitDuration);

		startPos = endPos;
		endPos = startPos + Vector3.up * 0.5f;
		for (float t = 0; t < hideDuration; t += Time.deltaTime) {
			float frac = t / hideDuration;
			transform.localPosition = Vector3.Lerp(startPos, endPos, frac);
			foreach (Image i in images) i.fillAmount = 1-frac;
			yield return new WaitForEndOfFrame();
		}

		gameObject.SetActive(false);
	}

	void Update () {
		transform.forward = Vector3.forward;
	}

	public static void PopupNumber (int number, Transform t, Vector3 offset, bool setParent) {
		GameObject g = Spawner.Spawn("NumberPopup", false);
		NumberController n = g.GetComponent<NumberController>();
		n.SetNumber(number);
		g.transform.position = t.position + offset;
		g.transform.SetParent(setParent ? t : Spawner.instance.transform);
		g.SetActive(true);
	}

	public static void PopupQuestion (Transform t, Vector3 offset, bool setParent) {
		GameObject g = Spawner.Spawn("QuestionPopup", false);
		g.transform.position = t.position + offset;
		g.transform.SetParent(setParent ? t : Spawner.instance.transform);
		g.SetActive(true);
	}
}
