﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class XPPopupController : MonoBehaviour {

	public Vector3 offset;
	public float delay = 0.5f;

	int diff;

	void OnEnable () {
		Gun.onXPChanged += OnXPChanged;
	}

	void OnDisable () {
		Gun.onXPChanged -= OnXPChanged;
	}

	void OnXPChanged(Gun gun) {
		int d = gun.xp - gun.prevXP;
		diff += d;
		StopCoroutine("DelayPopup");
		if (diff <= 0) return;
		StartCoroutine("DelayPopup");
	}

	IEnumerator DelayPopup () {
		yield return new WaitForSeconds(delay);
		PopupController.PopupNumber(diff, transform, offset, true);
		diff = 0;
	}
}
