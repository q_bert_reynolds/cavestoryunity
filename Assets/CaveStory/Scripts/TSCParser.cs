﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Paraphernalia.Components;

public class TSCParser : MonoBehaviour {

	public bool runTestOnStart = false;
	[Multiline(15)] public string testEvent;
	[ContextMenu("Run Test")]
	public void RunTest () {
		StartCoroutine("RunEventCoroutine", testEvent);
	}

	public static TSCParser instance;

	void Awake () {
		if (instance == null) {
			instance = this;
		}
	}

	void Start () {
		if (runTestOnStart) RunTest();
	}

	static float fps = 72;

	static string[] sounds = new string[] {
		"<nothing>", 							
		"[blip]", 								
		"typing", 								
		"bumpHead", 							
		"Weapon switch", 						
		"prompt", 								
		"critterJump", 							
		null, 									
		null, 									
		null, 									
		null, 									
		"openDoor", 							
		"blockBreak", 							
		null, 								
		"gainXP", 							
		"[click]", 							
		"hurt", 							
		"death", 							
		"confirm", 							
		null, 								
		"health", 							
		"[bubble]", 						
		"openedChest", 						
		"land", 							
		"step", 							
		"critterDeath", 					
		"landHard", 				
		"levelUp", 							
		"[thump]", 							
		"Teleport", 						
		"jump", 							
		"Invulnerable 'ting'", 					
		"shoot", 							
		"[shot - fireball]", 					
		"[shot - fireball bounce?]", 			
		"[explosion (missiles?]", 				
		null, 									
		"[click]", 								
		"gotItem", 								
		"[*bvng*]", 							
		"[water?]", 							
		"[water?]", 							
		"[beep]", 								
		"Computer beep", 						
		"Blast (blow up door to Shelt)", 		
		"bounceXP", 							
		"[*ftt*]", 								
		null, 									
		"[shot - bubble pop?]", 				
		"[shot - spur lv 1]", 					
		"[enemy squeak]", 						
		"critterHurt", 							
		"[large enemy take damage (roar)]", 	
		"[enemy squeak-bblblbl]", 				
		"gunHit", 								
		"[enemy squeak]", 						
		"splash", 								
		"[little damage sound]", 				
		"[*chlk*]", 							
		null, 									
		"[spur charge - lower]", 				
		"[spur charge - higher]", 				
		"Shot - Spur lv 2", 					
		"Shot - Spur lv 3", 					
		"Shot - Spur max", 						
		"Spur fully charged", 					
		null, 									
		null, 									
		null, 									
		null, 									
		"[Sue hitting you in Jail1]", 			
		"[explosion]", 							
		"[explosion]"							
	};

	static string[] maps = new string[] {
		"0", 
		"Pens1", 
		"Eggs", 
		"EggX", 
		"Egg6", 
		"EggR", 
		"Weed", 
		"Santa", 
		"Chako",
		"MazeI", 
		"Sand", 
		"Mimi", 
		"Cave", 
		"Start", 
		"Barr", 
		"Pool", 
		"Cemet", 
		"Plant",
		"Shelt", 
		"Comu", 
		"MiBox", 
		"EgEnd1", 
		"Cthu", 
		"Egg1", 
		"Pens2", 
		"Malco",
		"WeedS", 
		"WeedD", 
		"Frog", 
		"Curly", 
		"WeedB", 
		"Stream", 
		"CurlyS", 
		"Jenka1",
		"Dark", 
		"Gard", 
		"Jenka2", 
		"SandE", 
		"MazeH", 
		"MazeW", 
		"MazeO", 
		"MazeD",
		"MazeA", 
		"MazeB", 
		"MazeS", 
		"MazeM", 
		"Drain", 
		"Almond", 
		"River", 
		"Eggs2",
		"Cthu2", 
		"EggR2", 
		"EggX2", 
		"Oside", 
		"EgEnd2", 
		"Itoh", 
		"Cent", 
		"Jail1",
		"Momo", 
		"Lounge", 
		"CentW", 
		"Jail2", 
		"Blcny1", 
		"Priso1", 
		"Ring1", 
		"Ring2",
		"Prefa1", 
		"Priso2", 
		"Ring3", 
		"Little", 
		"Blcny2", 
		"Fall", 
		"Kings", 
		"Pixel",
		"e_Maze", 
		"e_Jenk", 
		"e_Malc", 
		"e_Ceme", 
		"e_Sky", 
		"Prefa2", 
		"Hell1", 
		"Hell2",
		"Hell3", 
		"Mapi", 
		"Hell4", 
		"Hell42", 
		"Statue", 
		"Ballo1", 
		"Ostep", 
		"e_Labo",
		"Pole", 
		"Island", 
		"Ballo2", 
		"e_Blcn", 
		"Clock"
	};

	static string[] mapNames = new string[] {
		"Credits", 
		"Arthur's House", 
		"Egg Corridor", 
		"Egg No. 00", 
		"Egg No. 06", 
		"Egg Observation Room", 
		"Grasstown", 
		"Santa's House", 
		"Chaco's House",
		"Labyrinth I", 
		"Sand Zone", 
		"Mimiga Village", 
		"First Cave", 
		"Start Point", 
		"Shack", 
		"Reservoir", 
		"Graveyard", 
		"Yamashita Farm",
		"Shelter", 
		"Assembly Hall", 
		"Save Point", 
		"Side Room", 
		"Cthulhu's Abode", 
		"Egg No. 01", 
		"Arthur's House - Sue on computer", 
		"Power Room",
		"Save Point", 
		"Execution Chamber", 
		"Gum", 
		"Sand Zone Residence", 
		"Grasstown Hut", 
		"Main Artery", 
		"Small Room", 
		"Jenka's House",
		"Deserted House", 
		"Sand Zone Storehouse", 
		"Jenka's House", 
		"Sand Zone", 
		"Labyrinth H", 
		"Labyrinth W", 
		"Camp", 
		"Clinic Ruins",
		"Labyrinth Shop", 
		"Labyrinth B", 
		"Boulder Chamber", 
		"Labyrinth M", 
		"Dark Place", 
		"Core", 
		"Waterway", 
		"Egg Corridor",
		"Cthulhu's Abode", 
		"Egg Observation Room", 
		"Egg No. 00", 
		"Outer Wall", 
		"Side Room", 
		"Storehouse", 
		"Plantation", 
		"Jail No. 1",
		"Hideout", 
		"Rest Area", 
		"Teleporter", 
		"Jail No. 2", 
		"Balcony", 
		"Last Cave", 
		"Throne Room", 
		"The King's Table",
		"Prefab House", 
		"Last Cave Hidden", 
		"Black Space", 
		"Little House", 
		"Balcony", 
		"Ending", 
		"Intro", 
		"Waterway Cabin",
		"Credits", 
		"Credits", 
		"Credits", 
		"Credits", 
		"Credits", 
		"Prefab House", 
		"Sacred Ground B1", 
		"Sacred Ground B2",
		"Sacred Ground B3", 
		"Storage", 
		"Passage", 
		"Passage", 
		"Statue Chamber", 
		"Seal Chamber", 
		"Corridor", 
		"Credits",
		"Hermit Gunsmith", 
		"", 
		"Seal Chamber", 
		"Credits", 
		"Clock Room"
	};

	public static string GetCurrentMapName () {
		string sceneName = SceneManager.GetActiveScene().name;
		return GetMapName(sceneName);
	}

	public static string GetMapName (string sceneName) {
		for (int i = 0; i < maps.Length; i++) {
			if (sceneName == maps[i]) return mapNames[i];
		}
		return "";
	}

	static int[] nonLoopedMusic = new int[] { 3, 10, 15, 16 };

	static bool IsLooped (int musicID) {
		foreach (int id in nonLoopedMusic) {
			if (musicID == id) return false;
		}
		return true;
	}

	static string currentMap;

	public static Coroutine RunEvent(string map, int eventID) {
		currentMap = map;
		string path = Path.Combine(Application.streamingAssetsPath, map + ".txt");
		string tscFile = File.ReadAllText(path);
		string[] events = tscFile.Split(new char[] {'#'});
		foreach (string e in events) {
			if (string.IsNullOrEmpty(e) || e.Length < 4) continue;
			
			string eID = e.Substring(0,4);
			if (int.Parse(eID) == eventID) {
				string content = e.Substring(4);
				return instance.StartCoroutine("RunEventCoroutine", content);
			}
		}
		return null;
	}

	static int ammoAdded;
	static AudioClip prevMusic;

	IEnumerator RunEventCoroutine (string e) {
		string[] commands = e.Split(new char[]{'<'});
		foreach (string command in commands) {
			if (string.IsNullOrEmpty(command) || command.Length < 3) continue;

			string type = command.Substring(0,3);
			string content = command.Substring(3);
			string[] split = content.Split(new char[]{':'});
			
			switch (type) {
				case "AE+":
					Debug.Log("TODO: <AE+         Arms Energy +          Refill ammo");
					break;
				case "AM+":
					ammoAdded = int.Parse(split[1]);
					Gun g = GameManager.GetGun(int.Parse(split[0]));
					g.maxAmmo += ammoAdded; 
					PlayerController.ReceiveGun(g);
					break;
				case "AM-":
					PlayerController.LoseGun(int.Parse(split[0]));
					break;
				case "AMJ":
					if (PlayerController.HasGun(int.Parse(split[0]))) {
						yield return RunEvent(currentMap, int.Parse(split[1]));
						yield break;
					}
					break;
				case "ANP":
					AnimatableCharacter animNPC = GameManager.FindAnimatableNPC(int.Parse(split[0]));
					if (animNPC) {
						animNPC.SetDirection(int.Parse(split[2]));
						animNPC.PlayAnimation(int.Parse(split[1]));
					}
					break;
				case "BOA":
					Debug.Log("TODO: <BOAx        BOss Animate           Animate boss");
					break;
				case "BSL":
					Debug.Log("TODO: <BSLx        Boss [Script Load?]    Start a boss fight with npc X (npc flag 0200 must be set) (should work with anything that has HP)");
					break;
				case "CAT":
					Debug.Log("TODO: <CAT         [?]                  @ [same as SAT?]");
					break;
				case "CIL":
					Debug.Log("TODO: <CIL         Clear ILlustration     Clear illustration during credits (used after SIL)");
					break;
				case "CLO":
					TextController.Close();
					break;
				case "CLR":
					TextController.Clear();
					if (!string.IsNullOrEmpty(content)) yield return TextController.Type(content);
					break;
				case "CMP":
					Debug.Log("TODO: <CMPx:y:z	Change map coords " + split[0] + ":" + split[1] + " to tile " + split[2]);
					break;
				case "CMU":
					prevMusic = AudioManager.currentMusic;
					int musicID = int.Parse(content.Substring(0,4));
					if (musicID == 0) AudioManager.StopMusic();
					else AudioManager.CrossfadeMusic(GameManager.instance.music[musicID], 0.1f, IsLooped(musicID));
					if (content.Length > 4) yield return TextController.Type(content.Substring(4));
					break;
				case "CNP":
					Interactable replacable = GameManager.FindNPC(int.Parse(split[0]));
					if (replacable == null) break;
					
					AnimatableCharacter replacement = GameManager.InstantiateNPC(int.Parse(split[1]));
					if (replacement) {
						replacement.GetComponent<Interactable>().eventID = replacable.eventID;
						replacement.transform.position = replacable.transform.position;
						replacement.SetDirection(int.Parse(split[2]));
					}
					DestroyImmediate(replacable.gameObject);
					break;
				case "CPS":
					Debug.Log("TODO: <CPS         Clear Prop. Sound      Stop propeller sound (used after SPS) (from helicopter cutscene after final battles)");
					break;
				case "CRE":
					Debug.Log("TODO: <CRE         CREdits                Roll credits");
					break;
				case "CSS":
					Debug.Log("TODO: <CSS         Clear Stream Sound     Stop stream sound (used after SSS) (from River area)");
					break;
				case "DNA":
					Debug.Log("TODO: <DNAx        [?]                    [something to do with bosses]");
					break;
				case "DNP":
					GameManager.DestroyNPC(int.Parse(split[0]));
					break;
				case "ECJ":
					Debug.Log("TODO: <ECJx:y      [EC?] Jump           @ Jump to event Y if any npc with ID X is present");
					break;
				case "END":
					Time.timeScale = 1;
					TextController.Close();
					PlayerController.instance.inputLocked = false;
					break;
				case "EQ+":
					Debug.Log("TODO: <EQ+x        EQuip +                Add X to equip flag bytes");
					break;
				case "EQ-":
					Debug.Log("TODO: <EQ-x        EQuip -                Subtract X from equip flag bytes");
					break;
				case "ESC":
					GameManager.QuitToMenu();
					break;
				case "EVE":
					Debug.Log("TODO: <EVEx        EVEnt                  Jump to event X (non-conditional)");
					break;
				case "FAC":
					int faceID = int.Parse(content.Substring(0,4));
					if (faceID == 0) TextController.HideFace();
					else TextController.ShowFace(GameManager.GetFace(faceID));
					if (content.Length > 4) yield return TextController.Type(content.Substring(4));
					break;
				case "FAI":
					yield return TransitionGraphic.FadeIn((TransitionGraphic.Style)int.Parse(content));
					break;
				case "FAO":
					yield return TransitionGraphic.FadeOut((TransitionGraphic.Style)int.Parse(content));
					break;
				case "FL+":
					PlayerController.AddFlag(int.Parse(content));
					break;
				case "FL-":
					PlayerController.RemoveFlag(int.Parse(content));
					break;
				case "FLA":
					Debug.Log("TODO: <FLA         FLAsh                  Flash the screen");
					break;
				case "FLJ":
					if (PlayerController.HasFlag(int.Parse(split[0]))) {
						yield return RunEvent(currentMap, int.Parse(split[1]));
						yield break;
					}
					break;
				case "FMU":
					AudioManager.FadeMusicVolume(0.1f, 0.1f);
					break;
				case "FOB":
					Debug.Log("TODO: <FOBx:y      Focus On Boss          [Focus view on boss X? why not use FON?], view movement takes Y ticks");
					break;
				case "FOM":
					float playerLerpTime = float.Parse(content) / fps;
					Debug.Log("TODO: Camera lerp should take " + playerLerpTime + " seconds to look at Player");
					CameraController.instance.offset = new Vector3(0, 0, -13);
					CameraController.instance.target = PlayerController.instance.transform;
					break;
				case "FON":
					Interactable npc = GameManager.FindNPC(int.Parse(split[0]));
					float npcLerpTime = float.Parse(split[1]) / fps;
					Debug.Log("TODO: Camera lerp should take " + npcLerpTime + " seconds to look at NPC " + npc);
					if (npc != null) {
						CameraController.instance.offset = new Vector3(0, 0, -13);
						CameraController.instance.target = npc.transform;
					}
					break;
				case "FRE":
					Debug.Log("TODO: <FRE         FREe                   Frees menu cursor [also used after ZAM for some reason?]");
					break;
				case "GIT":
					int graphicID = int.Parse(content.Substring(0, 4));
					if (graphicID == 0) {
						TextController.instance.itemBox.SetActive(false);
					}
					else {
						TextController.instance.itemUIImage.sprite = GameManager.GetItemIcon(graphicID);
						TextController.instance.itemBox.SetActive(true);
					}		
					if (content.Length > 4) yield return TextController.Type(content.Substring(4));
					break;
				case "HMC":
					PlayerController.instance.gameObject.SetActive(false);
					break;
				case "INI":
					Debug.Log("TODO: <INI         INItialize             Resets memory and starts game from the beginning");
					break;
				case "INP":
					Debug.Log("TODO: <INPx:y:z    [I?] NPc             @ Change npc X to npc type Y with direction Z with setting Flag 0x0100 [It seems that it setting the 'No Player Damage' Flag and not the Costum 0x0100 Flag]");
					break;
				case "IT+":
					PlayerController.ReceiveItem(int.Parse(content.Substring(0, 4)));
					if (content.Length > 4) yield return TextController.Type(content.Substring(4));
					break;
				case "IT-":
					PlayerController.LoseItem(int.Parse(content));
					break;
				case "ITJ":
					if (PlayerController.HasItem(int.Parse(split[0]))) {
						yield return RunEvent(currentMap, int.Parse(split[1]));
						yield break;
					}
					break;
				case "KEY":
					Time.timeScale = 1;
					TextController.instance.playerUI.SetActive(false);
					PlayerController.instance.inputLocked = true;
					break;
				case "LDP":
					SavePoint.Load();
					break;
				case "LI+":
					PlayerController.instance.GetComponent<HealthController>().health += int.Parse(content);
					break;
				case "ML+":
					PlayerController.instance.GetComponent<HealthController>().maxHealth += int.Parse(content);		
					break;
				case "MLP":
					Debug.Log("TODO: <MLP         Map [LP?]              Display map");
					break;
				case "MM0":
					Debug.Log("TODO: <MM0         My Motion 0            Instantly halts your horizontal motion");
					break;
				case "MNA":
					LocationText.ShowCurrent();
					break;
				case "MNP":
					Interactable moveNPC = GameManager.FindNPC(int.Parse(split[0]));
					moveNPC.transform.position = new Vector2(float.Parse(split[1]) + 0.5f, -float.Parse(split[2]) - 1);
					moveNPC.SetDirection(int.Parse(split[3]));
					break;
				case "MOV":
					PlayerController.SetMapPosition(new Vector2(
						float.Parse(split[0]), float.Parse(split[1]))
					);
					break;
				case "MP+":
					Debug.Log("TODO: <MP+x        [MaP ??]             @ [map-related]");
					break;
				case "MPJ":
					Debug.Log("TODO: <MPJx        [MaP Jump?]            [Jump to event X if map exists for current area?  the single instance of this seems to be dummied out]");
					break;
				case "MS2":
					Debug.Log("TODO: <MS2         MeSsage 2              Open invisible text box at top of screen (text follows)");
					break;
				case "MS3":
					Debug.Log("TODO: <MS3         MeSsage 3              Open normal text box at top of screen (text follows)");
					break;
				case "MSG":
					TextController.Open();
					yield return TextController.Type(content);
					break;
				case "MYB":
					PlayerController.Bump(int.Parse(content));
					break;
				case "MYD":
					PlayerController.SetDirection(int.Parse(content));
					break;
				case "NCJ":
					Debug.Log("TODO: <NCJx:y      [NpC?] Jump            Jump to event Y if any npc of type X is present");
					break;
				case "NOD":
					yield return TextController.Nod();
					yield return TextController.Type(content);
					break;
				case "NUM":
					Debug.Log("TODO: <NUM0000     [Number?]              Used to output Y from AM+ as text");
					break;
				case "PRI":
					Time.timeScale = 0;
					PlayerController.instance.inputLocked = true;
					break;
				case "PS+":
					Debug.Log("TODO: <PS+x:y      [P? Slot?]             Set teleporter slot X to location Y");
					break;
				case "QUA":
					CameraShake.MainCameraShake(0.1f, float.Parse(content) / fps);
					break;
				case "RMU":
					AudioManager.CrossfadeMusic(prevMusic, 0.1f);
					break;
				case "SAT":
					Debug.Log("TODO: <SAT         Speed-up All Text      Instant text display on all messages until END (glitches scrolling text)");
					break;
				case "SIL":
					Debug.Log("TODO: <SILx        Show ILlustration      Show illustration during credits (use CIL after)");
					break;
				case "SK+":
					Debug.Log("TODO: <SK+x        SKipflag +             Set skipflag X (remains set until program exits, to avoid repeating cutscenes/dialogue after retrying)");
					break;
				case "SK-":
					Debug.Log("TODO: <SK-x        SKipflag -?          @ Clear skipflag");
					break;
				case "SKJ":
					Debug.Log("TODO: <SKJx:y      SKipflag Jump          Jump to event Y if skipflag X is set");
					break;
				case "SLP":
					Debug.Log("TODO: <SLP         [Show? Location? P?]   Teleporter location menu");
					break;
				case "SMC":
					PlayerController.instance.gameObject.SetActive(true);
					break;
				case "SMP":
					Debug.Log("TODO: <SMPx:y      [?]                    [do something with npc X? - only used before and after the Omega fight]");
					break;
				case "SNP":
					Debug.Log("TODO: <SNPx:y:z:w  [Start?] NPc         @ Create npc type X at coords Y:Z with direction W");
					break;
				case "SOU":
					int soundEffectID = int.Parse(content.Substring(0, 4));
					string soundEffect = sounds[soundEffectID];
					AudioManager.PlayEffect(soundEffect);
					if (content.Length > 4) yield return TextController.Type(content.Substring(4));
					break;
				case "SPS":
					Debug.Log("TODO: <SPS         Start Prop. Sound      Start propeller sound (use CPS after) (from helicopter cutscene after final battles)");
					break;
				case "SSS":
					Debug.Log("TODO: <SSSx        Start Stream Sound     Start stream sound at pitch X (use CSS after) (from River area - normal pitch is 0400)");
					break;
				case "STC":
					Debug.Log("TODO: <STC         Save Time Counted      Saves the current time to 290.rec");
					break;
				case "SVP":
					SavePoint.Save();
					break;
				case "TAM":
					Debug.Log("TODO: <TAMx:y:z    Trade ArMs             Trade weapon X for weapon Y, set max ammo to Z (max ammo 0000 = no change) (GLITCH: first weapon 0000)");
					break;
				case "TRA":
					TextController.Close();
					string map = maps[int.Parse(split[0])];
					yield return SceneManager.LoadSceneAsync(map);
					PlayerController.SetMapPosition(new Vector2(
						float.Parse(split[2]), float.Parse(split[3]))
					);
					yield return RunEvent(map, int.Parse(split[1]));
					break;
				case "TUR":
					TextController.Print(content);
					break;
				case "UNI":
					Debug.Log("TODO: <UNIx        [?]                    [0000 normal / 0001 zero-g movement, facing direction is locked (disables focus commands) (from Stream boss) / 0002 movement is locked, can still fire]");
					break;
				case "UNJ":
					Debug.Log("TODO: <UNJx        [?] Jump?              [?] ");
					break;
				case "WAI":
					yield return new WaitForSecondsRealtime(float.Parse(content) / fps);
					break;
				case "WAS":
					Debug.Log("TODO: <WAS         WAit until Standing    Pause script until your character touches the ground");
					break;
				case "XX1":
					Debug.Log("TODO: <XX1x        [?]                    [shows distant view of island?]");
					break;
				case "YNJ":
					Debug.Log("TODO: <YNJx        Yes/No Jump            Ask yes or no, jump to event X if No");
					break;
				case "ZAM":
					Debug.Log("TODO: <ZAM         Zero ArMs              All weapons drop to level 1");
					break;
				default:
					Debug.Log(type + " command not implemented.");
					break;
			}
			yield return new WaitForEndOfFrame();
		}
	}
}
