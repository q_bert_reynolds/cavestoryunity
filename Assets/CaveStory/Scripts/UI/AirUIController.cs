﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AirUIController : MonoBehaviour {

	[Range(0, 1)] public float tickDuration = 0.1f;
	public float flashDuration = 2;
	public Image onesDigit;
	public Image tensDigit;
	public Image hundDigit;

	public Image airText;
	public Sprite warningSprite;
	public Sprite normalSprite;

	void Awake () {
		onesDigit.enabled = false;
		tensDigit.enabled = false;
		hundDigit.enabled = false;
		airText.enabled = false;
	}
	
	void OnEnable () {
		WaterController.onPlayerEnterWater += StartCountDown;
		WaterController.onPlayerExitWater += StopCountDown;
	}

	void OnDisable () {
		WaterController.onPlayerEnterWater -= StartCountDown;
		WaterController.onPlayerExitWater -= StopCountDown;
	}

	void StartCountDown () {
		StopCoroutine("Flash100");
		StartCoroutine("CountDown");
	}

	void StopCountDown () {
		StopCoroutine("CountDown");
		StartCoroutine("Flash100");
	}

	IEnumerator CountDown () {
		onesDigit.enabled = true;
		airText.enabled = true;
		for (int i = 99; i > 0; i--) {
			onesDigit.sprite = GameManager.GetNumberSprite(i % 10);
			tensDigit.enabled = i >= 10;
			tensDigit.sprite = GameManager.GetNumberSprite(i / 10);
			hundDigit.enabled = i == 100;
			airText.sprite = ((2*i)%6 > 3) ? warningSprite : normalSprite;
			yield return new WaitForSeconds(tickDuration);			
		}
	}

	IEnumerator Flash100 () {
		onesDigit.enabled = true;
		tensDigit.enabled = true;
		hundDigit.enabled = true;
		airText.enabled = true;
		onesDigit.sprite = GameManager.GetNumberSprite(0);
		tensDigit.sprite = GameManager.GetNumberSprite(0);
		airText.sprite = normalSprite;
		for (float t = 0; t < flashDuration; t += Time.deltaTime) {
			onesDigit.enabled = !onesDigit.enabled;
			tensDigit.enabled = !tensDigit.enabled;
			hundDigit.enabled = !hundDigit.enabled;
			yield return new WaitForEndOfFrame();
			yield return new WaitForEndOfFrame();
		}
		onesDigit.enabled = false;
		tensDigit.enabled = false;
		hundDigit.enabled = false;
		airText.enabled = false;
	}
}
