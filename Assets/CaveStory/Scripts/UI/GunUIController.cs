﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GunUIController : MonoBehaviour {
	
	public Image gunIcon;
	public Image xpBar;
	public Image maxXPText;
	public Image levelNumber;
	public Image ammoOnesDigit;
	public Image ammoTensDigit;
	public Image ammoHundDigit;
	public Image clipOnesDigit;
	public Image clipTensDigit;
	public Image clipHundDigit;

	void OnEnable () {
		Gun.onXPChanged += OnXPChanged;
		PlayerController.onGunChanged += OnGunChange;
		OnGunChange(PlayerController.currentGun);
	}

	void OnXPChanged(Gun gun) {
		xpBar.fillAmount = gun.progress;
		bool isMax = gun.xp >= gun.maxXP;
		maxXPText.enabled = isMax;
		xpBar.enabled = !isMax;
		levelNumber.sprite = GameManager.GetNumberSprite(gun.currentLevel);
	}

	public void OnGunChange (Gun gun) {
		if (gun) {
			gunIcon.enabled = true;
			gunIcon.sprite = gun.icon;
			OnXPChanged(gun);
		}
		else {
			gunIcon.enabled = false;
			xpBar.fillAmount = 0;
			maxXPText.enabled = false;
			levelNumber.sprite = GameManager.GetNumberSprite(0);
		} 
	}
}
