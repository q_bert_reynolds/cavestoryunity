﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class HealthUIController : MonoBehaviour {

	public Image healthBar;
	public Image onesDigit;
	public Image tensDigit;

	private HealthController healthController;
	
	void Start () {
		if (PlayerController.instance == null) return;
		HealthController healthController = PlayerController.instance.GetComponent<HealthController>();
		OnHealthChanged(healthController, healthController.health, healthController.health, healthController.maxHealth);
	}

	void OnEnable () {
		SceneManager.sceneLoaded += OnSceneLoad;
		HealthController.onAnyHealthChanged += OnHealthChanged;
	}

	void OnDisable () {
		SceneManager.sceneLoaded += OnSceneLoad;
	}

	void OnSceneLoad (Scene scene, LoadSceneMode mode) {
		if (PlayerController.instance == null) return;
		HealthController healthController = PlayerController.instance.GetComponent<HealthController>();
		OnHealthChanged(healthController, healthController.health, healthController.health, healthController.maxHealth);
	}

	void OnHealthChanged(HealthController hc, float health, float prevHealth, float maxHealth) {
		if (hc.gameObject.tag != "Player") return;
		healthBar.fillAmount = health / maxHealth;
		int h = Mathf.Max(0, Mathf.RoundToInt(health));
		onesDigit.sprite = GameManager.GetNumberSprite(h % 10);
		tensDigit.enabled = h >= 10;
		tensDigit.sprite = GameManager.GetNumberSprite(h / 10);
	}
}
