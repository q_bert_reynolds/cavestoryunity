﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LocationText : MonoBehaviour {

	public static LocationText instance;
	public float duration = 2;

	Text text;

	void Awake () {
		text = GetComponent<Text>();

		if (instance == null) {
			instance = this;
			text.enabled = false;
		}
	}

	public static void DoorOpened(string name) {
		instance.StopCoroutine("Show");
		instance.text.text = name;
		instance.StartCoroutine("Show");
	}

	public static void ShowCurrent() {
		instance.StopCoroutine("Show");
		instance.text.text = TSCParser.GetCurrentMapName();;
		instance.StartCoroutine("Show");
	}

	IEnumerator Show () {
		text.enabled = true;
		yield return new WaitForSecondsRealtime(duration);
		text.enabled = false;
	}
}
