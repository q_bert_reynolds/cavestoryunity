﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using Paraphernalia.Components;

public class MainMenuController : MonoBehaviour {

	public Transform selector;

	GameObject lastSelection;
	public void SelectionChanged () {
		GameObject g = EventSystem.current.currentSelectedGameObject;
		selector.transform.position = g.transform.position;
		if (g != lastSelection && lastSelection != null) {
			AudioManager.PlayEffect("select");
		}
		lastSelection = g;
	}

	public void NewGame () {
		AudioManager.PlayEffect("confirm");
		SceneManager.LoadScene("NewGame");
	}

	public void LoadGame () {
		AudioManager.PlayEffect("confirm");
		SavePoint.Load();
	}	
}
