﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NumberController : MonoBehaviour {

	public Image signImage;
	public Image onesDigit;
	public Image tensDigit;
	public Image hundDigit;

	public void SetNumber(int n) {
		signImage.sprite = GameManager.GetSignSprite(n);
		int abs = Mathf.Abs(n);
		int sign = (int)Mathf.Sign(n);
		onesDigit.sprite = GameManager.GetNumberSprite(n % 10, sign);
		tensDigit.gameObject.SetActive(abs >= 10);
		tensDigit.sprite = GameManager.GetNumberSprite((n / 10) % 10, sign);
		hundDigit.gameObject.SetActive(abs >= 100);
		hundDigit.sprite = GameManager.GetNumberSprite((n / 100) % 10, sign);
	}
}
