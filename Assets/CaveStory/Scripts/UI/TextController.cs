﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Paraphernalia.Components;

public class TextController : MonoBehaviour {

	public static TextController instance;
	public static bool isDisplaying {
		get { return instance.uiText.enabled == true; }
	}

	public delegate void YesNoCallback (bool answer);
	public delegate void TextCallback ();

	Queue<IEnumerator> textQueue = new Queue<IEnumerator>();

	public string cursor = "█";
	public Image bgUIImage;
	public Image itemUIImage;
	public Image face;
	public Text uiText;
	public GameObject yesNoBox;
	public GameObject itemBox;
	public GameObject playerUI; 
	public RectTransform finger;
	public float scrollSpeed = 1;
	public bool resetTimeScale = true;

	RectTransform textRect;

	void Awake () {
		if (instance == null) {
			instance = this;
		}
	}
	void Start () {
		textRect = uiText.GetComponent<RectTransform>();
		uiText.font.material.mainTexture.filterMode = FilterMode.Point;
		StartCoroutine(ShowTextCoroutine());
	}

	public static void Open () {
		instance.submitPressed = false;
		instance.uiText.text = "";
		instance.uiText.enabled = true;
		instance.bgUIImage.enabled = true;
		instance.yesNoBox.SetActive(false);
		instance.itemBox.SetActive(false);
		instance.playerUI.SetActive(false);
		HideFace();
	}

	public static void Close () {
		instance.Cleanup();
	}

	public static void Clear () {
		instance.uiText.text = "";
	}

	public static Coroutine Nod () {
		return instance.StartCoroutine(instance.FlashCursorAndWait());
	}

	public static Coroutine Type (string text) {
		if (string.IsNullOrEmpty(text)) return null;
		return instance.StartCoroutine(instance.AddText(text));		
	}

	public static void Print (string text) {
		if (!string.IsNullOrEmpty(instance.uiText.text)) text = "\n" + text;
		instance.uiText.text += text;		
	}

	public static void ShowFace (Sprite sprite) {
		instance.face.enabled = true;
		instance.face.sprite = sprite;
		instance.textRect.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 54, 177);
	}

	public static void HideFace () {
		instance.face.enabled = false;
		instance.textRect.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 0, 231);
	}

	void Setup () {
		Time.timeScale = 0;
		Open();
	}

	void Cleanup () {
		uiText.enabled = false;
		bgUIImage.enabled = false;
		Time.timeScale = 1;
		yesNoBox.SetActive(false);
		itemBox.SetActive(false);
		playerUI.SetActive(true);
		HideFace();
	}

	public static void ShowText (string text) {
		instance.textQueue.Enqueue(instance.RevealTextAndWait(text));
	}

	public static void ShowText (string[] texts) {
		instance.textQueue.Enqueue(instance.RevealTextsWithWaits(texts));
	}

	public static void AskYesNo (string question, YesNoCallback callback) {
		instance.textQueue.Enqueue(instance.AskYesNoAndWait(question, callback));
	}

	public static void DisplayItem (Sprite sprite, string[] texts, TextCallback GotItemCallback, TextCallback FinishCallback, AudioClip music = null) {
		instance.textQueue.Enqueue(instance.DisplayItemAndWait(sprite, texts, GotItemCallback, FinishCallback, music));
	}

	IEnumerator ShowTextCoroutine () {
		Cleanup();
		while (enabled) {
			if (textQueue.Count > 0) {
				Setup();
				while (textQueue.Count > 0) {
					yield return StartCoroutine(textQueue.Dequeue());
				}
				Cleanup();
			}
			yield return new WaitForEndOfFrame();
		}
	}

	IEnumerator RevealTextAndWait (string text) {
		yield return StartCoroutine(RevealText(text));
		yield return StartCoroutine(FlashCursorAndWait());
	}

	IEnumerator RevealTextsWithWaits (string[] texts) {
		yield return StartCoroutine(RevealTextAndWait(texts[0]));
		for (int i = 1; i < texts.Length; i++) {
			yield return StartCoroutine(AddTextAndWait("\n" + texts[i]));
		}
	}

	IEnumerator AddTextAndWait (string text) {
		yield return StartCoroutine(AddText(text));
		yield return StartCoroutine(FlashCursorAndWait());
	}

	IEnumerator AddTextsWithWaits (string[] texts) {
		for (int i = 0; i < texts.Length; i++) {
			yield return StartCoroutine(AddTextAndWait("\n" + texts[i]));
		}
	}

	IEnumerator DisplayItemAndWait (Sprite sprite, string[] texts, TextCallback GotItemCallback, TextCallback FinishCallback, AudioClip music = null) {
		AudioClip prevMusic = AudioManager.currentMusic;
		AudioManager.CrossfadeMusic(music, 0.1f, false);
		itemUIImage.sprite = sprite;
		itemBox.SetActive(true);
		if (GotItemCallback != null) GotItemCallback();
		yield return StartCoroutine(RevealTextAndWait(texts[0]));
		AudioManager.CrossfadeMusic(prevMusic, 0.1f);
		for (int i = 1; i < texts.Length; i++) {
			yield return StartCoroutine(AddTextAndWait("\n" + texts[i]));
		}
		if (FinishCallback != null) FinishCallback();
		itemBox.SetActive(false);
	}

	IEnumerator RevealText (string text) {
		uiText.text = "";
		yield return StartCoroutine(AddText(text));
	}

	IEnumerator AddText (string text) {
		text = text.Replace('=', '●');
		string startText = uiText.text;
		for (int i = 1; i < text.Length; i++) {
			uiText.text = startText + text.Substring(0, i+1);
			AudioManager.PlayEffect("typing");
			float wait = (Input.GetButton("Submit")) ? 0.025f : 0.1f;
			yield return new WaitForSecondsRealtime(wait);
		}
	}

	IEnumerator AskYesNoAndWait (string question, YesNoCallback Callback) {
		yield return StartCoroutine(RevealText(question));
		submitPressed = false;
		yesNoBox.SetActive(true);
		AudioManager.PlayEffect("prompt");
		
		Debug.Log("TODO: lerp yes/no box up to position");

		bool choice = true;
		finger.anchoredPosition = new Vector2(4, 14);
		while (!submitPressed) {
			if (!choice && x < 0) {
				finger.anchoredPosition = new Vector2(4, 14);
				choice = true;
				AudioManager.PlayEffect("select");
			}
			else if (choice && x > 0) {
				finger.anchoredPosition = new Vector2(44, 14);
				choice = false;
				AudioManager.PlayEffect("select");
			}
			yield return new WaitForEndOfFrame();
		}
		AudioManager.PlayEffect("confirm");
		if (Callback != null) Callback(choice);
		submitPressed = false;
		yesNoBox.SetActive(false);
	}

	IEnumerator FlashCursorAndWait () {
		submitPressed = false;
		string text = uiText.text;
		bool showCursor = true;
		while (!submitPressed) {
			uiText.text = text + (showCursor ? cursor : "");
			showCursor = !showCursor;
			yield return new WaitForSecondsRealtime(0.2f);			
		}
		submitPressed = false;
		uiText.text = text;
	}

	bool submitPressed;
	int x;
	void Update () {
		if (Input.GetButtonDown("Submit")) submitPressed = true;
		x = Mathf.RoundToInt(Input.GetAxisRaw("Horizontal"));

		int lines = uiText.cachedTextGenerator.lineCount;
		if (lines <= 3) {
			textRect.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, 0, 48);
		}
		else {
			float offset = 16 * (lines - 3);
			float inset = Mathf.Lerp(textRect.offsetMax.y, -offset, scrollSpeed);
			textRect.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, inset, 48 + offset);
		}
	}
}
