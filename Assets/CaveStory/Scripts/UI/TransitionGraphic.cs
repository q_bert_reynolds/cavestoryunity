﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class TransitionGraphic : Graphic {

	public static TransitionGraphic instance;

	public enum State {	Off, FadeOut, Opaque, FadeIn }
	public State state = State.Off;

	public enum Style { Left=0, Up=1, Right=2, Down=3, Center=4}
	public Style style = Style.Left;

	public float stepDuration = 0.05f;
	
	int index;
	int rowCenter;
	int colCenter;

	public override Texture mainTexture {
		get { return material.mainTexture; }
	}

	protected override void Awake () {
		base.Awake();
		if (instance == null) instance = this;
	}

	void FadeInOut (VertexHelper vh) {
		UIVertex vert = UIVertex.simpleVert;
		vert.color = color;
		Vector3[] corners = new Vector3[4];
		rectTransform.GetLocalCorners(corners);
		float w = corners[2].x - corners[0].x;
		float h = corners[2].y - corners[0].y;
		float x = corners[0].x;
		float y = corners[0].y;
		int columns = Mathf.CeilToInt(w / 16f);
		int rows = Mathf.CeilToInt(h / 16f);
		rowCenter = rows / 2;
		colCenter = columns / 2;
		int tile = 0;
		for (int col = 0; col < columns; col++) {
			for (int row = 0; row < rows; row++) {
				float i = x + col * 16;
				float j = y + row * 16;
			
				Rect r = GetUV(row, col);
				vert.position = new Vector2(i, j);
				vert.uv0 = new Vector2(r.xMin, r.yMin);
				vh.AddVert(vert);

				vert.position = new Vector2(i, j+16);
				vert.uv0 = new Vector2(r.xMin, r.yMax);
				vh.AddVert(vert);

				vert.position = new Vector2(i+16, j+16);
				vert.uv0 = new Vector2(r.xMax, r.yMax);
				vh.AddVert(vert);

				vert.position = new Vector2(i+16, j);
				vert.uv0 = new Vector2(r.xMax, r.yMin);
				vh.AddVert(vert);

				vh.AddTriangle(tile, tile+1, tile+2);
				vh.AddTriangle(tile, tile+2, tile+3);
				tile += 4;
			}
		}
	}

	void Opaque (VertexHelper vh) {
		UIVertex vert = UIVertex.simpleVert;
		vert.color = color;
		
		Vector3[] corners = new Vector3[4];
		rectTransform.GetLocalCorners(corners);

		vert.position = corners[0];
		vert.uv0 = new Vector2(0, 0);
		vh.AddVert(vert);

		vert.position = corners[1];
		vert.uv0 = new Vector2(0, 1);
		vh.AddVert(vert);

		vert.position = corners[2];
		vert.uv0 = new Vector2(0.0625f, 1);
		vh.AddVert(vert);

		vert.position = corners[3];
		vert.uv0 = new Vector2(0.0625f, 0);
		vh.AddVert(vert);

		vh.AddTriangle(0, 1, 2);
		vh.AddTriangle(0, 2, 3);
	}

    protected override void OnPopulateMesh(VertexHelper vh) {
        vh.Clear();
		switch (state) {
			case State.FadeIn:
			case State.FadeOut:
				FadeInOut(vh);
				break;
			case State.Opaque:
				Opaque(vh);
				break;
			default:
				break;
		}
    }

	Rect GetUV(int row, int col) {
		int offset = ((int)style < 2) ? col : row;
		int i = Mathf.Clamp(index + offset, 0, 15);
		if ((state == State.FadeOut) != ((int)style % 2 == 1)) i = 15 - i;
		
		if (style == Style.Center) {
			int x = Mathf.Abs(row - rowCenter);
			int y = Mathf.Abs(col - colCenter);
			offset = x + y;
			i = (state == State.FadeOut) ? 15 - index - offset : index - offset;
			i = Mathf.Clamp(i, 0, 15);
		}
		
		Rect r = new Rect(0.0625f * i, 0, 0.0625f, 1);
		return r;
	}

	public static Coroutine FadeIn(Style s) {
		instance.state = State.FadeIn;
		instance.style = s;
		return instance.StartCoroutine("FadeInOutCoroutine");
	}

	public static Coroutine FadeOut(Style s) {
		instance.state = State.FadeOut;
		instance.style = s;
		return instance.StartCoroutine("FadeInOutCoroutine");
	}

	IEnumerator FadeInOutCoroutine () {
		bool posStyle = (int)style % 2 == 0;
		if (posStyle) yield return StartCoroutine(PositiveStyleFade());
		else yield return StartCoroutine(NegativeStyleFade());
		state = (state == State.FadeIn) ? State.Off : State.Opaque;
		UpdateGeometry();
	}

	IEnumerator PositiveStyleFade () {
		for (index = -16; index < 16; index++) {
			UpdateGeometry();
			yield return new WaitForSecondsRealtime(stepDuration);
		}
	}

	IEnumerator NegativeStyleFade () {
		for (index = 16; index > -16; index--) {
			UpdateGeometry();
			yield return new WaitForSecondsRealtime(stepDuration);
		}
	}
}
