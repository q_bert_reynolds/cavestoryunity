﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnscaledTimeParticles : MonoBehaviour {

	ParticleSystem particles;

	void Awake () {
		particles = GetComponent<ParticleSystem>();
	}

	void Update () {
		particles.Simulate(Time.unscaledDeltaTime, true, false);	
	}
}
