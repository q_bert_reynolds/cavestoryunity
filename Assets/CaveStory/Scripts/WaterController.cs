﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Paraphernalia.Components;

public class WaterController : MonoBehaviour {

	public delegate void OnWaterEvent();
	public static event OnWaterEvent onPlayerEnterWater = delegate{};
	public static event OnWaterEvent onPlayerExitWater = delegate{};

	public string splashParticle = "Splash";
	void OnTriggerEnter2D (Collider2D collider) {
		if (collider.gameObject.tag == "Player") {
			onPlayerEnterWater();
			PlayerController player = collider.gameObject.GetComponent<PlayerController>();
			player.inWater = true;
			Rigidbody2D body = collider.gameObject.GetComponent<Rigidbody2D>();
			Vector2 v = body.velocity;
			v.y *= -1;
			if (v.y > 1) {
				ParticleManager.Play(splashParticle, body.position, v);
				AudioManager.PlayEffect("splash");
			}
		}
	}

	void OnTriggerExit2D (Collider2D collider) {
		if (collider.gameObject.tag == "Player") {
			onPlayerExitWater();
			PlayerController player = collider.gameObject.GetComponent<PlayerController>();
			player.inWater = false;
		}
	}
}
