TODO:
    first Balrog fight
    more weapons
    weapon switching
    pause menu
    map system
    fix audio (shouldn't have fadouts at the end of looped tracks)
    reprocess images
        extrude Prt* tiles by 1px (ie. margin=1, spacing=2)
        read and repack standard spritesheets with 2px transparent margin
    water death

BUGS:
    die, don't continue - player UI stays on screen, can't change selection
    player jumps when NOD is last event

NOTES:
    pausing/text sets time scale to 0
    cutscenes should use unscaled time
    walkthrough video - https://youtu.be/LaHEjxmMKB0
    original game save files stored here:
        ~/Library/Preferences/com.nakiwo.Doukutsu.plist
    remake save files stored here:
        ~/Library/Application\ Support/Shreveport\ Arcade/Cave\ Story/save.json
